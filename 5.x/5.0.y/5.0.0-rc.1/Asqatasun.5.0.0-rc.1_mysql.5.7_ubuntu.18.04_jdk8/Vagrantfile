# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what you're doing.
ENV["LC_ALL"] = "en_US.UTF-8"
Vagrant.configure("2") do |config|

  # Default values
  default_cpus_number = 1     #    1 |    2 |    3 |    4
  default_memory_size = 2048  # 2048 | 4096 | 6144 | 8192
  default_host_api_ip   = "127.0.0.1"
  default_host_app_ip   = "127.0.0.1"
  default_host_db_ip    = "127.0.0.1"
  default_host_api_port = 8081
  default_host_app_port = 8080
  default_host_db_port  = 3306
  default_asqatasun_version = "5.0.0-rc.1"
  default_firefox_version = "78.2.0esr"
  default_geckodriver_version = "0.26.0"

  # Environment variable customizations
  cpus_number   = ENV['VAGRANT_CPU']      ? ENV['VAGRANT_CPU']      : default_cpus_number
  memory_size   = ENV['VAGRANT_MEMORY']   ? ENV['VAGRANT_MEMORY']   : default_memory_size
  host_api_ip   = ENV['VAGRANT_API_IP']   ? ENV['VAGRANT_API_IP']   : default_host_api_ip
  host_app_ip   = ENV['VAGRANT_APP_IP']   ? ENV['VAGRANT_APP_IP']   : default_host_app_ip
  host_db_ip    = ENV['VAGRANT_DB_IP']    ? ENV['VAGRANT_DB_IP']    : default_host_db_ip
  host_api_port = ENV['VAGRANT_API_PORT'] ? ENV['VAGRANT_API_PORT'] : default_host_api_port
  host_app_port = ENV['VAGRANT_APP_PORT'] ? ENV['VAGRANT_APP_PORT'] : default_host_app_port
  host_db_port  = ENV['VAGRANT_DB_PORT']  ? ENV['VAGRANT_DB_PORT']  : default_host_db_port
  asqatasun_version   = ENV['VAGRANT_ASQATASUN_VERSION']   ? ENV['VAGRANT_ASQATASUN_VERSION']   : default_asqatasun_version
  firefox_version     = ENV['VAGRANT_FIREFOX_VERSION']     ? ENV['VAGRANT_FIREFOX_VERSION']     : default_firefox_version
  geckodriver_version = ENV['VAGRANT_GECKODRIVER_VERSION'] ? ENV['VAGRANT_GECKODRIVER_VERSION'] : default_geckodriver_version

  # Virtual Machine OS
  config.vm.box = "ubuntu/bionic64" # Ubuntu 18.04

  # Provider-specific configuration
  config.vm.provider "virtualbox" do |vb|

    # Virtual Machine Name
    vb.name = "Asqatasun.#{asqatasun_version}_ubuntu.18.04_MySQL"

    # Allowed memory
    # ---> increase this value if you want to use API and webapp at the same time
    vb.memory = memory_size  # 2048 | 4096 | 6144 | 8192

    # Number of virtual CPUs for the virtual machine.
    # ---> increase this value if you want to use API and webapp at the same time
    vb.cpus = cpus_number

    # Enable linked clones based on a master VM (reduce overhead in terms of time)
    vb.linked_clone = true if Gem::Version.new(Vagrant::VERSION) >= Gem::Version.new('1.8.0')
  end

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Enable provisioning with a shell script.
  config.vm.provision :shell, :inline => <<-SH
    # By using inline scripts you can also pass values into the provisioning
    # step where they can be leveraged by your provisioning scripts.
    set -eux
    export ASQATASUN_VERSION_FROM_VAGRANTFILE=#{asqatasun_version}
    export FIREFOX_VERSION_FROM_VAGRANTFILE=#{firefox_version}
    export GECKODRIVER_VERSION_FROM_VAGRANTFILE=#{geckodriver_version}
    export WEBAPP_IP_FROM_VAGRANTFILE=#{host_app_ip}
    export WEBAPP_PORT_FROM_VAGRANTFILE=#{host_app_port}
    source /vagrant/bootstrap.sh
  SH

  # If you need to export or import an existing database,
  # we recommend that you install vagrant-disksize plugin
  # and uncomment the following line
  # config.disksize.size = '40GB'

  # Host name of the VM
  config.vm.hostname = "asqsatasun-vagrant"

  # Network configuration
  ###############################################################################################
  ###############################################################################################

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine.
  # ---> Asqatasun webapp   port 8080
  # ---> Asqatasun API      port 8081
  # ---> MySQL server       port 3306
  # ---> MailHog            port 8025
  config.vm.network "forwarded_port", id: 'Asqatasun-webapp', guest: 8080, host: host_app_port, host_ip: host_app_ip
  config.vm.network "forwarded_port", id: 'Asqatasun-API',    guest: 8081, host: host_api_port, host_ip: host_api_ip
  config.vm.network "forwarded_port", id: 'API management',   guest: 8091, host: 8091,          host_ip: "127.0.0.1", auto_correct: true
  config.vm.network "forwarded_port", id: 'MySQL',            guest: 3306, host: host_db_port,  host_ip: host_db_ip
  config.vm.network "forwarded_port", id: 'MailHog',          guest: 8025, host: 8025,          host_ip: "127.0.0.1", auto_correct: true

  # Shared folders: Host <--> Guest (VM)
  ###############################################################################################
  ###############################################################################################

  # Default share folder:  Host  "./"  <--> Guest (VM) "/vagrant"
  #
  # Share an additional folder to the guest VM.
  # - The first argument is the path on the host to the actual folder.
  # - The second argument is the path on the guest to mount the folder.
  # - And the optional third argument is a set of non-required options.
  config.vm.synced_folder "./shared_download", "/home/vagrant/download", created: true
# config.vm.synced_folder "./shared_logs",     "/var/log/asqatasun",     created: true

  # Message to show after vagrant up
  ###############################################################################################
  ###############################################################################################
  config.vm.post_up_message = <<-MESSAGE
  ------------------------------------------------------
  Gecko driver ... #{geckodriver_version}
  Firefox ........ #{firefox_version}
  Asqatasun ...... #{asqatasun_version}
  ------------------------------------------------------


  ------------------------------------------------------
  /!\\ Created for testing purpose only, 
      no security has been made for production.
  ------------------------------------------------------

  # Start/Stop API server service
  vagrant ssh -c '00_bin/60_start_API_service.sh'
  vagrant ssh -c '00_bin/61_stop_API_service.sh'

  # Display log of API server service
  vagrant ssh -c '00_bin/62_display-logs_API_service.sh'

  ------------------------------------------------------

  # Start/Stop webapp service
  vagrant ssh -c '00_bin/50_start_WEBAPP_service.sh'
  vagrant ssh -c '00_bin/51_stop_WEBAPP_service.sh'

  # Display log of webapp service
  vagrant ssh -c '00_bin/52_display-logs_WEBAPP_service.sh'

 ------------------------------------------------------

  # Start API server manually + see log directly  (use Ctrl-C to stop it)
  vagrant ssh -c '00_bin/10_start_API.sh'

  # Start webapp manually     + see log directly  (use Ctrl-C to stop it)
  vagrant ssh -c '00_bin/00_start_WEBAPP.sh'

  ------------------------------------------------------
  Use 'vagrant port' command line to see port mapping.

  Default :  3306 (guest) --> #{host_db_port} (host)  Database port (MySQL server)    
             8081 (guest) --> #{host_api_port} (host)  API      port
             8080 (guest) --> #{host_app_port} (host)  Webapp   port               
  ------------------------------------------------------
  Database   .... jdbc:mysql://#{host_db_ip}:#{host_db_port}/asqatasun
  URL API    .... http://#{host_api_ip}:#{host_api_port}
  URL webapp .... http://#{host_app_ip}:#{host_app_port}
  ------------------------------------------------------
  MESSAGE
  ###############################################################################################
  ###############################################################################################
end
