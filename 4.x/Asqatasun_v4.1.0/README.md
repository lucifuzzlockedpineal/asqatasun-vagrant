# Asqatasun v4.1.0 Box on Ubuntu 18.04

> ⚠️   Created for test purposes only, no security has been made for production. ⚠️

## 0. Pre-requisites

* Install plugin [vagrant-disksize](https://github.com/sprotheroe/vagrant-disksize): `vagrant plugin install vagrant-disksize`

## 1. Outsite the box

Get into this directory, then:

```
vagrant up
vagrant ssh
```

## 2. Inside the box

```
sudo -i
cd /vagrant
./asqatasun.sh
```

Several minutes later... 
the script ends with a `tail -f` on Asqatasun and Tomcat log files

## 3. Use it

* In your browser, go to `http://localhost:8087/asqatasun/`
* Use this user and this password :
  - `admin@asqatasun.org`
  - `myAsqaPassword`

## 4. Destroy the box

When you're done, from outside the box:

```
vagrant destroy -f
```

