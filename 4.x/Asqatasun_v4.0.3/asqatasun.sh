#!/bin/bash

set -o errexit

#############################################
# Variables
#############################################

declare ASQA_RELEASE="4.0.3"
declare ASQA_DOWNLOAD_URL="https://github.com/Asqatasun/Asqatasun/releases/download/v${ASQA_RELEASE}/asqatasun-${ASQA_RELEASE}.i386.tar.gz"

declare TOMCAT_URL="http://localhost:8080"
declare ASQA_URL="${TOMCAT_URL}/asqatasun/"
declare ASQA_ADMIN_EMAIL="me@my-email.org"
declare ASQA_ADMIN_PASSWD="myAsqaPassword"

declare DATABASE_DB="asqatasun"
declare DATABASE_HOST="localhost"
declare DATABASE_USER="asqatasun"
declare DATABASE_PASSWD="asqaP4sswd"
declare TOMCAT_WEBAPP_DIR="/var/lib/tomcat7/webapps"
declare TOMCAT_USER="tomcat7"

# Set fonts for Help
BOLD=$(tput bold)
UNDR=$(tput smul)
REV=$(tput rev)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
NORM=$(tput sgr0)
NORMAL=$(tput sgr0)

#############################################
# Functions
#############################################

# display a step message
function display_step_msg() {
    echo "-------------------------------------------------------"
    echo "${1}"
}

# Check URL (return HTTP code)
function check_URL() {
    local URL="$1"
    set +e
    local RESULT=$(curl -o /dev/null --silent --write-out '%{http_code}\n' "${URL}")
    set -e
    echo "${RESULT}"
}

# Test if TOMCAT_URL responds with an HTTP 200 code
function check_Tomcat_loading() {
    local  time=0
    local  RESULT=$(check_URL "${TOMCAT_URL}")
    while (( ${RESULT} != 200 ))
    do
        RESULT=$(check_URL ${TOMCAT_URL})
        if [[ "${RESULT}" == "200" ]]; then
            display_step_msg "Tomcat server is now running .......... HTTP code = ${BOLD}${GREEN}${RESULT}${NORM}"
        else
            ((time+=1))
            echo " ... ${REV}${time}${NORM} ... Tomcat server loading ..."
            sleep 1
        fi
    done
}

# Install Puppet + other packages
install_packages() {
    display_step_msg "Installing packages"
    sudo apt-get update
    sudo apt-get install -y \
        ccze \
        curl
}

# Set locale because of bug in 14.04 + 16.04
set_locale() {
    display_step_msg "Defining locale"
    sudo echo 'LC_ALL="en_US.UTF-8"' >>/etc/environment
    sudo echo 'LC_ALL="en_US.UTF-8"' >>/etc/default/locale
    sudo echo 'LANG="en_US.UTF-8"' >>/etc/default/locale
}

# Download Asqatasun and extract archive
download_asqatasun_and_extract() {
    display_step_msg "Downloading Asqatasun"
    cd /root
    wget -q ${ASQA_DOWNLOAD_URL}
    tar xvfz asqatasun-*tar.gz
    mv asqatasun*/ ./asqatasun/
}

# Install and configure Asqatasun v4.0.3
install_asqatasun_v4.0.3() {
    display_step_msg "Installing Asqatasun v4.0.3 --> ${BOLD}${GREEN}pre-requisites${NORM}"
    cd /root/asqatasun/install/

    # Remove interactive commands (moreover useless) of pre-requisites.sh
    sed -i '/apt-get clean/d' pre-requisites.sh
    sed -i '/apt-get autoremove/d' pre-requisites.sh
    sed -i '/rm -rf \/var\/lib\/apt\/lists\/\*/d' pre-requisites.sh

    ./pre-requisites.sh

    # Prevently stop Tomcat
    service tomcat7 stop

    display_step_msg "Installing Asqatasun v4.0.3 --> ${BOLD}${GREEN}install${NORM}"
    cd /root/asqatasun/
    echo "yes" | ./install.sh               \
        --database-db       ${DATABASE_DB}       \
        --database-host     ${DATABASE_HOST}     \
        --database-user     ${DATABASE_USER}     \
        --database-passwd   ${DATABASE_PASSWD}   \
        --asqatasun-url     ${ASQA_URL}          \
        --tomcat-webapps    ${TOMCAT_WEBAPP_DIR} \
        --tomcat-user       ${TOMCAT_USER}       \
        --asqa-admin-email  ${ASQA_ADMIN_EMAIL}  \
        --asqa-admin-passwd ${ASQA_ADMIN_PASSWD} \
        --firefox-esr-binary-path /opt/firefox/firefox \
        --display-port      :99
}

# Install and configure Asqatasun
install_asqatasun() {
    display_step_msg "Installing Asqatasun"
    if [[ "${ASQA_RELEASE}" == "4.0.3" ]]; then
        install_asqatasun_v4.0.3
    else
        echo "Unkonwn Asqatasun release"
        echo "exiting"
        exit -1
    fi
}

# Post install actions
post_install() {
    display_step_msg "Running post-install actions"

    # To avoid bug at first launch (Asqatasun < 4.0.4)
    touch /var/log/asqatasun/asqatasun.log
    chown "${TOMCAT_USER}" /var/log/asqatasun/asqatasun.log
    service tomcat7 start

    echo "========================================================================="
    echo ""
    echo "OK wait a few seconds", *then*:
    echo "Go to http://localhost:8404/asqatasun/"
    echo ""
    echo "========================================================================="

    check_Tomcat_loading
}

#############################################
# Main
#############################################

install_packages
set_locale
download_asqatasun_and_extract
install_asqatasun
post_install