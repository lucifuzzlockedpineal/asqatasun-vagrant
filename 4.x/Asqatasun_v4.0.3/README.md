# Asqatasun Box on Ubuntu 14.04

## 0. Define Asqatasun release

Edit `asqatasun.sh` and change value of `ASQA_RELEASE`.

## 1. Outsite the box

Get into this directory, then:

```
vagrant up
vagrant ssh
```

## 2. Inside the box

```
sudo -i
cd /vagrant
./asqatasun.sh
```

## 3. Destroy the box

From outsite the box:

```
vagrant destroy -f
```