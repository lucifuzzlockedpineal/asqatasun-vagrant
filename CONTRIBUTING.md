# Contributing

## Versioning

- All notable changes to this project will be documented in [CHANGELOG](CHANGELOG.md) file.
- This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

    ```shell
    MAJOR.MINOR.PATCH
       # MAJOR ---> a breaking change: incompatible changes (ex: reorganization of directories)
       # MINOR ---> add a new feature: a new Docker image (ex: new Asqtasun version)
       # PATCH ---> fix a bug
    ```

## Git Commit message convention

We follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) :

```xml
<type>[(optional scope)]: <description>

[optional body]

[optional footer(s)]
```

> common types:
>
> - `build`: changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm).
> - `ci`: changes to the CI configuration files and scripts
> - `chore`: update something without impacting the user (ex: bump a dependency in composer.json).
> - `docs`: documentation only changes
> - `feat`: add a new feature (equivalent to a `MINOR` in Semantic Versioning)
> - `fix`: fix a bug (equivalent to a `PATCH` in Semantic Versioning).
> - `perf`: a code change that improves performance.
> - `refactor`: a code change that neither fixes a bug nor adds a feature.
> - `revert`: revert a commit.
> - `style`: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc).
> - `test`: adding missing tests, refactoring tests; no production code change.

The commit message should follow this regex:

```perl
/^(revert: )?(build|ci|chore|docs|feat|fix|perf|refactor|style|refactor|revert|style|test|)(\(.+\))?: .{1,50}/
```
